import React, { useState } from 'react';
import { Worker, Viewer } from '@react-pdf-viewer/core';
import Box from '@mui/material/Box';

export default function App() {
  const [sample, setSample] = useState(1)
  console.log(process.env.REACT_FILE_URL);

  return (
    <>
    <button onClick={() => setSample(sample+1)}>{`${process.env.REACT_FILE_URL}${sample}.pdf`}</button>
    <Worker workerUrl="https://unpkg.com/pdfjs-dist@2.6.347/build/pdf.worker.min.js">
      <Box style={{ height: '600px' }}>
        <Viewer
          defaultScale={1}
          // fileUrl="https://prod-assignments.s3.ap-south-1.amazonaws.com/student/mgmt11learn156/23558/1633438144000.pdf"
          fileUrl={`${process.env.REACT_APP_URL}${sample}.pdf`}
        />
      </Box>
    </Worker>
    </>
  );
}

